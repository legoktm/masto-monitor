## masto-monitor

Rudimentary anti-spam bot that watches the federated timeline for
specific keywords and sends automated reports for them.

### Compiling

You will need a [Rust toolchain](https://rustup.rs/) to be installed. Then after checking out this code, run:
```
$ cargo build --release
$ ./target/release/masto-monitor
```

The `masto-monitor` binary is statically compiled and can be copied/moved as desired.

### Configuration

To generate an OAuth access token, go to your Mastodon settings -> Development -> New application.

Fill in a name, you can leave website blank and the "Redirect URI" as the prefilled `oob` value. Check
the boxes for the `read` and `write:reports` scopes. After submitting, you should be able to view your
application, which shows you an access token.

### Specifying keywords

The keywords to look for match against the HTML of a post, which contains different markup. The best way
to understand what it looks like is to view the API response of your instance's timeline at `/api/v1/timelines/public`.

For example, if you want to report whenever a specific user is pinged, you cannot match against `@example`. Mentions
look like: `<a href=\"https://social.example/@example\" class=\"u-url mention\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">@<span>example</span></a>`.

So you could set `@<span>example</span>` as a keyword to report on.

### Operation

Copy the `config.toml.example` file to `config.toml` and fill in the necessary information. `masto-monitor` will run indefinitely,
polling your instance's federated timeline every 30 seconds. If you want to make changes to the configuration, it must be restarted.

## Reuse

Licensed under the GPL v3, or any later version. The initial scaffolding was
written by [ChatGPT 3.5](https://chat.openai.com/share/5b220e4f-e660-456e-a658-9f91d472f8ea) and presumably, public domain.

Forking and customizing for your instance's needs is strongly encouraged.
